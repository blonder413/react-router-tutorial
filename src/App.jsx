import { Navigate, Route, Routes } from 'react-router-dom';
import { NavBarComponent } from "./routes/components/NavBarComponent";
import { AboutScreen } from './routes/AboutScreen';
import { ContactScreen } from './routes/ContactScreen';
import { HomeScreen } from './routes/HomeScreen';

export const App = () => {
    return (
        <>
            <NavBarComponent></NavBarComponent>
            <Routes>
                <Route path='/' element={<HomeScreen></HomeScreen>}></Route>
                <Route path='/about' element={<AboutScreen></AboutScreen>}></Route>
                <Route path='/contact' element={<ContactScreen></ContactScreen>}></Route>
                <Route path='/*' element={<Navigate to='/'></Navigate>}></Route>
            </Routes>
        </>
    );
};
